from mio import readJson, readCSV, flatten

class Pokemon:
    def __init__(self, pDict):
        # basic
        self.id = pDict["id"]
        self.name = pDict["name"]
        self.type = pDict["type"]
        self.evolutions = []
        self.evolve = None
        # stats
        self.hp = pDict["base"]["HP"]
        self.atk = pDict["base"]["Attack"]
        self.dfn = pDict["base"]["Defense"]
        self.satk = pDict["base"]["Sp. Attack"]
        self.sdfn = pDict["base"]["Sp. Defense"]
        self.spd = pDict["base"]["Speed"]
        self.statTotals()
        # moves
        self.moves = pDict["moves"]
        self.egg_moves = pDict["egg_moves"]
        self.tm_moves = pDict["tm_moves"]
        # etc
        self.description = pDict["description"]
        self.species = pDict["species"]
        self.weight = pDict["profile"]["weight"]
        self.height = pDict["profile"]["height"]
        self.egg = pDict["profile"]["egg"]
        self.gender = pDict["profile"]["gender"]
        self.image = pDict["image"]["hires"]
        self.sprite = pDict["image"]["sprite"]
        self.thumbnail = pDict["image"]["thumbnail"]


    def allMoves(self):
        egg = [[0,move] for move in self.egg_moves]
        tm =  [["TM"+move[0],move[1]] for move in self.tm_moves]
        return sorted(self.moves+egg+tm, key=lambda x:x[0])


    def statTotals(self):
        if self.atk > self.satk:
            self.attacker = "Physical"
            self.pst = self.atk + self.spd
        else:
            self.attacker = "Special"
            self.pst = self.satk + self.spd  
        self.bst = self.hp + self.atk + self.dfn + self.satk + self.sdfn + self.spd



    ##########################################
    @classmethod
    def loadPokedex(cls):
        pokedex = {}
        moveDex = {pokemon["name"]:pokemon for pokemon in readJson("resources/pokedex1.json")}
        baseDex = {pokemon["name"]:pokemon for pokemon in readJson("resources/pokedex2.json")}
            
        for name,pokemon in baseDex.items():
            # add moves
            if name in moveDex:
                pokemon["moves"] = moveDex[name]["level_up_moves"]
                pokemon["tm_moves"] = moveDex[name]["tms"]
                pokemon["egg_moves"] = moveDex[name]["egg_moves"]
                pokemon["abilities"] = [abi[0] for abi in pokemon["profile"]["ability"]]
            else:
                pokemon["moves"] = []
                pokemon["tm_moves"] = []
                pokemon["egg_moves"] = []
                pokemon["abilities"] = []

            # verify stats
            if "base" not in pokemon:
                if name not in moveDex: continue;
                pokemon["base"] = {
                    "HP":moveDex[name]["base_stats"][0],
                    "Attack":moveDex[name]["base_stats"][1],
                    "Defense":moveDex[name]["base_stats"][2],
                    "Sp. Attack":moveDex[name]["base_stats"][3],
                    "Sp. Defense":moveDex[name]["base_stats"][4],
                    "Speed":moveDex[name]["base_stats"][5]
                }
            # convert to pokemon object; add to GLOBAL dict
            try:
                pokedex[name] = cls(pokemon)
                pokedex[name.lower()] = pokedex[name]
                pokedex[pokemon["id"]] = pokedex[name]
            except:
                pass


        for name in baseDex: 
            try:
                thisPoke = pokedex[name]
                pokedex[name].evolutions = []
                evoData = baseDex[name]["evolution"]

                if "next" in evoData:
                    for e in evoData["next"]:
                        # extract data
                        evoMethod = e[1]
                        if "Level" in evoMethod: evoMethod = int(evoMethod.split()[-1])
                        nextId = int(e[0])
                        nextPoke = pokedex[nextId]
                        nextName = nextPoke.name
                        # update data
                        nextPoke.evolve = evoMethod
                        thisPoke.evolutions.append(nextPoke)
                        # thisPoke.evolutions[nextName] = nextPoke
            except:
                pass

        return pokedex