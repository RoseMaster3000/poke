#####################################
# POKEMON INFINITE FUSION
#####################################

from mio import readJson, readCSV, color, flatten
from pprint import pprint

# Resources
fused = flatten(readCSV("inputs/fused.csv"))
caught = flatten(readCSV("inputs/caught.csv"))
starters = flatten(readCSV("resources/starters.csv"))
legendary = flatten(readCSV("resources/legendary.csv"))
available = flatten(readCSV("resources/infinite-fusion.csv"))
pokedex = readJson("resources/pokedex0.json")
pokedict = {pokemon["name"]["english"]:pokemon for pokemon in pokedex}

# filter pokemon in game
# pokedict = {key:val for key,val in pokedict.items() if key in available}

# # filter starters
# pokedict = {key:val for key,val in pokedict.items() if key in starters}

# filter pokemon we caught
pokedict = {key:val for key,val in pokedict.items() if key in caught}

# # filter pokemon we have not fused
# pokedict = {key:val for key,val in pokedict.items() if key not in fused}

# # filter legendary pokemon
# pokedict = {key:val for key,val in pokedict.items() if key not in legendary}



# Crunch Fusions
fusions = []
for head in pokedict.keys():
    for body in pokedict.keys():
        bodyStats = pokedict[body]["base"]
        headStats = pokedict[head]["base"]
        # if head==body: continue;
        # body stats
        attack =   int(bodyStats["Attack"]*(2/3)      + headStats["Attack"]*(1/3))
        defense =  int(bodyStats["Defense"]*(2/3)     + headStats["Defense"]*(1/3))
        speed =    int(bodyStats["Speed"]*(2/3)       + headStats["Speed"]*(1/3))
        # head stats
        sAttack =  int(headStats["Sp. Attack"]*(2/3)  + bodyStats["Sp. Attack"]*(1/3))
        sDefense = int(headStats["Sp. Defense"]*(2/3) + bodyStats["Sp. Defense"]*(1/3))
        hp =       int(headStats["HP"]*(2/3)          + bodyStats["HP"]*(1/3))
        # best / practical stat totals
        if abs(sAttack-attack) < 15:
            tAttack = "Mixed" 
            goodAttack = int((sAttack + attack)/2)
        elif sAttack > attack:
            tAttack = "Special"
            goodAttack = sAttack
        else:
            tAttack = "Physical"
            goodAttack = attack
        pst = goodAttack+speed                         # practical stat total
        bst = attack+sAttack+defense+speed+sDefense+hp # base stat total
        if pokedict[head]['type'][0] == pokedict[body]['type'][-1]:
            types = pokedict[head]['type'][0] + " / " + pokedict[body]['type'][0]
        else:
            types = pokedict[head]['type'][0] + " / " + pokedict[body]['type'][-1]
        names = f"{head}+{body}"
        fusions.append((names,types,pst,  tAttack,  hp,attack,defense,sAttack,sDefense,speed,  bst))


# Sort / Display Fusions
fusions.sort(key=lambda x: x[2])
for f in fusions[-200:]:
    if f[3] == "Physical":
        color("red")
    elif f[3] == "Special":
        color("cyan")
    else:
        color("purple")
    statBlock = f[4:]
    print(f"{f[0]:<25}{f[1]:<20}{f[2]:<10} {statBlock}")
    color()