# Overview
This repository includes a collection of python scripts to make informed, data driven choices when playing pokemon.

### Installation
If you have not already, install the following
* Install Python `paru python` or download [here](https://www.python.org/downloads/)
* Install git `paru git` or download [here](https://git-scm.com/downloads)
* Install git LFS `paru git-lfs` or download [here](https://git-lfs.github.com/)

### Setup
1. Clone this repo `git clone <repo://url>`
2. Activate git LFS `git lfs install`
3. Clone Large Files `git lfs pull`
4. Run program `python reborn.py`

### Credits
Pokedex data collected from [here](https://github.com/Purukitto/pokemon-data.json).

# Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:shahros3@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|